<?php

namespace App\Http\Controllers;

use App\postingan;
use Illuminate\Http\Request;
// use App\Http\Controllers\Auth;
use Auth;

class PostinganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $post = postingan::all();
        return view('postingan.index', compact('post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('postingan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */ 
    public function store(Request $request)
    {
        //
        $this->validate($request,[
    		'postingan' => 'required',
    		// 'isi' => 'required',
            
    	]);
 
        postingan::create([
    		'postingan' => $request->postingan,
    		// 'isi' => $request->isi,
            'user_id' => Auth::id()
    	]);
 
    	return redirect('/postingan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\postingan  $postingan
     * @return \Illuminate\Http\Response
     */
    
    public function show($id)
    {
        //
        $post = postingan::find($id);
        return view('postingan.show', compact('post'));
        
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\postingan  $postingan
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $postingan = postingan::find($id);
        return view('postingan.edit', compact('postingan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\postingan  $postingan
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        //
        $request->validate([
            'postingan' => 'required|unique:post',
            // 'isi' => 'required',
        ]);

        $postingan = postingan::find($id);
        $postingan->postingan = $request->postingan;
        // $postingan->isi = $request->isi;
        $postingan->update();
        return redirect('/postingan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\postingan  $postingan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post = postingan::find($id);
        $post->delete();
        return redirect('/postingan');
    }
}
