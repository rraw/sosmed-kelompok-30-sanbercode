<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class postingan extends Model
{
    //
    protected $table = "postingan";
    protected $fillable = ["postingan", "user_id" ];
}
 