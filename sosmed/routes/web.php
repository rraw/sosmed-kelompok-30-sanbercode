<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('erd'); 
});


Route::get('/blank', function () {
    return view('layouts.master');
});

Route::get('/login', function () {
    return view('layouts.login');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// cek postingan
Route::resource('postingan', 'PostinganController');

Route::get('/postingan', 'PostinganController@index');
Route::get('/postingan/create', 'PostinganController@create');
Route::post('postingan', 'PostinganController@store');
Route::get('/postingan/{id}', 'PostinganController@show');
Route::get('/postingan/{id}/edit', 'PostinganController@edit');
Route::put('/postingan/{id}', 'PostinganController@update');
Route::delete('/postingan/{id}', 'PostinganController@destroy');
