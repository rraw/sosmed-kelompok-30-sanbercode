@extends('layouts.master')
@section('content')
    <a href="/postingan/create" class="btn btn-primary">Tambah</a>
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Postingan</th>
            {{-- <th scope="col">Isi</th> --}}
            {{-- <th scope="col">user</th> --}}
            <th scope="col" style="display: inline">Actions</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($post as $key=>$value)
                <tr>
                    <td>{{$key + 1}}</th>
                    <td>{{$value->postingan}}</td>
                    {{-- <td>{{$value->isi}}</td> --}}
                    {{-- <td>{{$value->id_user}}</td> --}}
                    <td>
                        <a href="/postingan/{{$value->id}}" class="btn btn-info">Show</a>
                        <a href="/postingan/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        <form action="/postingan/{{$value->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>  
            @endforelse              
        </tbody>
    </table>    
@endsection
