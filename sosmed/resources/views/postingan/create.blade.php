@extends('layouts.master')
@section('content')
    <form action="/postingan" method="POST">
        @csrf
        {{Auth::user()->id}} 
        <div class="form-group"> 
            <label for="title">Masukkan Post</label>
            <input type="text" class="form-control" name="postingan" id="postingan" placeholder="Masukkan post">
            @error('postingan')
                <div class="alert alert-danger">
                    {{$message}}
                </div>
            @enderror
        </div>
        {{-- <div class="form-group">
            <label for="body">Isi</label>
            <textarea name="isi" id="body" class="form-control" cols="30" rows="10"></textarea>
            @error('body')
                <div class="alert alert-danger">
                    {{$message}}
                </div>
            @enderror
        </div> --}}
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
@endsection

