@extends('layouts.master')
@section('content')
    <form action="/postingan/{{$postingan->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" name="judul" id="title" value="{{$postingan->postingan}}" placeholder="Masukkan POst">
                @error('title')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            {{-- <div class="form-group">
                <label for="body">Isi</label>
                <textarea name="isi" id="body" class="form-control" cols="30" rows="10">{{$postingan->isi}}</textarea>
                @error('body')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div> --}}
            <div class="form-group">
                <label for="user_id">user_id</label>
                <input disabled type="text" class="form-control" name="user_id" id="user_id" value="{{Auth::user()->id}}">
                @error('body')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Ubah</button>
        </div>
    </form>
@endsection